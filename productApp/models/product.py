# clase importada para creacion de modelo
from django.db import models

# clase creada para el modelo de producto
class Product(models.Model):
    #atributos del producto, que van a representar las columnas de la tabla
    id = models.AutoField(primary_key=True)
    name = models.CharField('NameProduct', max_length = 25)
    price = models.IntegerField(default=0)
    prove = models.CharField('NameProveedor',max_length=25)
    descr = models.TextField('Desscripcion',max_length=250)