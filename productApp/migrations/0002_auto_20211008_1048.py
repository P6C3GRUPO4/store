# Generated by Django 3.2.8 on 2021-10-08 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('productApp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='descr',
            field=models.TextField(default='a', max_length=250, verbose_name='Desscripcion'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='prove',
            field=models.CharField(default='a', max_length=25, verbose_name='NameProveedor'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(max_length=25, verbose_name='NameProduct'),
        ),
    ]
