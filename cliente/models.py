# clase importada para creacion de modelo
from django.db import models

# clase creada para el modelo de cliente
class Cliente(models.Model):
    #atributos del cliente, que van a representar las columnas de la tabla
    id = models.AutoField(primary_key=True)
    cnames = models.CharField('NombreCliente', max_length = 30)
	caplld = models.CharField('ApellidoCliente', max_length = 30)
    cident = models.IntegerField(default=0)
    cemail = models.CharField('emailCliente',max_length=30)
    ctelef = models.CharField('PhoneCliente',max_length=12)